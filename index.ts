import fetch, { Request, RequestInit } from "node-fetch"
import * as semver from "semver"
import * as sendgrid from "@sendgrid/mail"
import * as firebaseAdmin from "firebase-admin"
import * as firebaseFunctions from "firebase-functions"

sendgrid.setApiKey(process.env.SENDGRID_API_KEY!)

firebaseAdmin.initializeApp(firebaseFunctions.config().firebase)

var db = firebaseAdmin.firestore()

function http<T>(url: string | Request, init?: RequestInit): Promise<T> {
  return fetch(url, init).then((resp) => resp.json())
}

type AURInfo = {
  type: "multiinfo"
  version: number
  resultcount: number
  results: Array<{
    ID: number
    Name: string
    Version: string
    OutOfDate: number | null
  }>
}

type GitHubReleasesQueryResponse = {
  data: {
    repository: {
      releases: {
        edges: Array<{
          cursor: string
          node: {
            name: string
            tagName: string
            isDraft: boolean
            isPrerelease: boolean
          }
        }>
      }
    }
  }
}

type LastNotifiedVersionDocument = {
  "brave-bin"?: string
}

async function emailOutOfDateNotification(
  packageName: string,
  latestVersion: string,
  aurVersion: string,
) {
  return sendgrid.send({
    to: process.env.NOTIFICATION_EMAIL!,
    from: process.env.FROM_EMAIL!,
    subject: `${packageName} AUR package is out of date`,
    text: `Latest release: ${latestVersion}. AUR package at version: ${aurVersion}`,
  })
}

let lastNotifiedVersionDocument: LastNotifiedVersionDocument | null = null

async function getLastNotifiedVersion(
  packageName: keyof LastNotifiedVersionDocument,
): Promise<string | undefined> {
  if (lastNotifiedVersionDocument == null) {
    const docRef = db
      .collection("aur-out-of-date-notifications")
      .doc("last-versions-notified")
    const docSnapshot = await docRef.get()
    const doc = docSnapshot.data()
    if (doc === undefined) {
      await docRef.set({})
      lastNotifiedVersionDocument = {}
    } else {
      lastNotifiedVersionDocument = (doc as any) as LastNotifiedVersionDocument
    }
  }
  return Promise.resolve(lastNotifiedVersionDocument[packageName])
}

async function setLastNotifiedVersion<
  PN extends keyof LastNotifiedVersionDocument,
  V extends LastNotifiedVersionDocument[PN]
>(packageName: PN, version: V): Promise<void> {
  return db
    .collection("aur-out-of-date-notifications")
    .doc("last-versions-notified")
    .update({ [packageName]: version })
    .then(() => {
      return
    })
}

async function queryGitHubReleases(
  owner: string,
  name: string,
  limit: number = 50,
) {
  const gqlQuery = `
    query {
      repository(owner: "${owner}", name: "${name}") {
        releases(last:${limit}) {
          edges {
            cursor
            node {
              name
              tagName
              isDraft
              isPrerelease
            }
          }
        }
      }
    }`
  return http<GitHubReleasesQueryResponse>("https://api.github.com/graphql", {
    method: "POST",
    headers: {
      Authorization:
        "Basic " +
        Buffer.from(
          process.env.GITHUB_USER + ":" + process.env.GITHUB_PERSONAL_TOKEN,
        ).toString("base64"),
    },
    body: JSON.stringify({ query: gqlQuery }),
  })
}

async function checkBrave(): Promise<void> {
  const newestBraveVersion = await fetch(
    "https://brave-browser-downloads.s3.brave.com/latest/release.version",
  ).then((res) => res.text())

  const aurInfoList = await http<AURInfo>(
    "https://aur.archlinux.org/rpc/?v=5&type=info&arg[]=brave-bin",
  )
  const aurInfo = aurInfoList.results.find((i) => i.Name === "brave-bin")
  if (aurInfo === undefined) {
    throw new Error("package not found")
  }
  const lastAURVersionDashAt = aurInfo.Version.lastIndexOf("-")
  const aurSemVer =
    aurInfo.Version.substring(
      0,
      lastAURVersionDashAt === -1
        ? aurInfo.Version.length
        : lastAURVersionDashAt,
    ).substring(aurInfo.Version.indexOf(":") + 1)

  if (semver.gt(newestBraveVersion, aurSemVer)) {
    await emailOutOfDateNotification("brave-bin", newestBraveVersion, aurSemVer)
  }
}

export async function checkEverything() {
  await checkBrave()
}

export async function runGCF(data: any, context: any, callback: any) {
  await checkEverything();
  callback()
}
